"""main gui module"""

import ffmpyg
import gi
from multiprocessing import Process
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
from gi.repository import Gtk, GLib

class Gui:
    """Main GUI class."""
    def __init__(self):
        """init docstring"""
        builder = Gtk.Builder()
        builder.add_from_file("glade/main.glade")
        #get objects from glade file
        self.window = builder.get_object("window")
        # filechooserbutton
        self.filechooserbutton = builder.get_object("filechooserbutton")
        # cut switch button
        self.switch_cut = builder.get_object("switch_cut")
        self.switch_cut.connect("notify::active", self.on_switch_cut_activate)
        # cut start spin button
        self.spinbutton_start = builder.get_object("spinbutton_start")
        # cut duration spin button
        self.spinbutton_duration = builder.get_object("spinbutton_duration")
        # preset combobox
        self.combobox_preset = builder.get_object("combobox_preset")
        # crf combobox
        self.combobox_crf = builder.get_object("combobox_crf")
        # audio combobox
        self.combobox_audio = builder.get_object("combobox_audio")
        self.hbox_cut = builder.get_object("hbox_cut")
        # scale switch button
        self.switch_scale = builder.get_object("switch_scale")
        self.switch_scale.connect("notify::active", self.on_switch_scale_activate)
        # scale height combobox
        self.combobox_height = builder.get_object("combobox_scale_height")
        self.hbox_scale = builder.get_object("hbox_scale")
        self.btn_encode = builder.get_object("btn_encode")
        self.spinner = builder.get_object("spinner")

        builder.connect_signals(self)
        self.window.connect("delete-event", Gtk.main_quit)
        self.window.show_all()
        self.hbox_cut.hide()
        self.hbox_scale.hide()

    def on_switch_cut_activate(self, switch, data=None):
        """on switch activate docstring"""
        if switch.get_active():
            self.hbox_cut.show()
        else:
            self.hbox_cut.hide()

    def on_switch_scale_activate(self, switch, data=None):
        """on_switch_scale_activate"""
        if switch.get_active():
            self.hbox_scale.show()
        else:
            self.hbox_scale.hide()

    def on_btn_encode_clicked(self, data=None):
        """On button encode clicked method."""
        in_filename = self.filechooserbutton.get_filename()
        if not in_filename:
            print(in_filename)
            self.simple_message_dialog("Debe seleccionar un archivo primero.", 1)
            return
        else:
            options = {}
            liststore_preset = self.combobox_preset.get_model()
            options["preset"] = liststore_preset[self.combobox_preset.get_active_iter()][0]
            liststore_crf = self.combobox_crf.get_model()
            options["crf"] = liststore_crf[self.combobox_crf.get_active_iter()][0]
            liststore_audio = self.combobox_audio.get_model()
            audio_value = liststore_audio[self.combobox_audio.get_active_iter()][0]

            if audio_value == "no audio":
                options["audio"] = False
            elif audio_value == "copy":
                options["audio"] = "copy"
            else:
                options["audio"] = audio_value

            if self.switch_scale.get_active():
                options["scale"] = True
                liststore_height = self.combobox_height.get_model()
                options["height"] = liststore_height[self.combobox_height.get_active_iter()][0]
            else:
                options["scale"] = False
            if self.switch_cut.get_active():
                start = self.spinbutton_start.get_value_as_int()
                duration = self.spinbutton_duration.get_value_as_int()
                if start >= 0 and duration > 0:
                    options["cut"] = True
                    options["start"] = str(start)
                    options["duration"] = str(duration)
                else:
                    options["cut"] = False
            else:
                options["cut"] = False

            ffmpeg = ffmpyg.Ffwrapper()
            process = Process(target=ffmpeg.h264_aac_encode, args=(in_filename, options))
            process.start()
            # start spinner
            self.spinner.start()
            self.btn_encode.set_sensitive(False)
            GLib.timeout_add(1000, self.check_process, process)

    def check_process(self, process):
        """encode process cycling timeout checker"""
        #print("checking encode process in timeout function")
        if process.is_alive():
            return True
        else:
            if process.exitcode == 0:
                self.simple_message_dialog("File transcoded succesfuly")
            else:
                self.simple_message_dialog("File transcoding failed.", 1,
                                           "Exit code: {}".format(process.exitcode))
            self.spinner.stop()
            self.btn_encode.set_sensitive(True)
            return False

    def simple_message_dialog(self, message, msg_type=0, extra_text=""):
        """Simple message dialog."""
        if msg_type == 0:
            msg_type = Gtk.MessageType.INFO
        elif msg_type == 1:
            msg_type = Gtk.MessageType.ERROR
        elif msg_type == 2:
            msg_type = Gtk.MessageType.WARNING

        dialog = Gtk.MessageDialog(self.window, 0, msg_type, Gtk.ButtonsType.OK, message)
        if extra_text:
            dialog.format_secondary_text(extra_text)
        dialog.run()
        dialog.destroy()

if __name__ == "__main__":
    gui = Gui()
    Gtk.main()
